import requests
import os
from dotenv import load_dotenv
load_dotenv()

use_auth = False
inference_url = 'https://api.wikimedia.org/service/lw/inference/v1/models/langid:predict'

if use_auth:
    headers: {
      'Authorization': 'Bearer '+os.getenv('ACCESS_TOKEN'),
      'User-Agent': 'ZentralGut.ch',
      'Content-type': 'application/json'
  }
else:
    headers = {}

def getFileUrl(file):
    #file = 'File:ZHB_SoSa_Portrait_127_cropped.jpg'
    base_url = 'https://api.wikimedia.org/core/v1/commons/file/'
    url = base_url + file
    response = requests.get(url, headers=headers)
    return (response.json())

if __name__ == '__main__':
    print(getFileUrl("File:ZHB SoSa Portrait 004 cropped.jpg"))