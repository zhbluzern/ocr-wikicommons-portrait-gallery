import requests

def getOCR(fileUrl):

    apiUrl = "https://ocr.wmcloud.org/api"
    params = { "engine" : "google", 
            "image" : fileUrl}
    headers = { 'accept' : '*/*', 'User-Agent': 'ZentralGut.ch'}
    response = requests.get(url=apiUrl, params=params, headers=headers)
    return response.json()

if __name__ == '__main__':
    print(getOCR("https://upload.wikimedia.org/wikipedia/commons/5/56/ZHB_SoSa_Portrait_001_cropped.jpg"))

