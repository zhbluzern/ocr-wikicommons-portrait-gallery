import getPortraitsFromCat as getPortraits
import getCommonsFileUrl as getCommonsUrl
import getOCRfromImage as getOCR
import getLanguage as getLanguage
import pandas as pd
import json

portraits = (getPortraits.getCroppedPortraits())
resultSet = []

for portrait in portraits:
    resultDet = {}
    print(portrait)
    resultDet["portrait"] = portrait
    fileUrls = getCommonsUrl.getFileUrl(portrait)
    resultDet["fileUrl"] = fileUrls["original"]["url"]
    print(fileUrls["original"]["url"])
    ocr = getOCR.getOCR(fileUrls["original"]["url"])
    try:
        print(ocr["text"])
        resultDet["text"] = ocr["text"]
        language = getLanguage.getLanguage(ocr["text"])
        print(language)
        resultDet["language"] = language["language"]
        resultDet["languageName"] = language["languagename"]
        resultDet["languageScore"] = language["score"]
    except KeyError:
        print("no text recognised")
    
    resultSet.append(resultDet)
    #break

#Schreibe Metadaten (resultSet) in ein XLS-File zur Weiterbearbeitung
df = pd.DataFrame(resultSet) 
print(df)
#df.to_excel('Output_Lido.xlsx') 
df.to_csv('Output.csv')

# Schreibe Metadaten (resultSet) in ein JSON-File
json_str = json.dumps(resultSet)
# And then write that string to a file
with open('Output.json', 'w') as f:
    f.write(json_str)