import requests
import json
import os
from dotenv import load_dotenv
load_dotenv()

use_auth = False

if use_auth:
    headers: {
      'Authorization': 'Bearer '+os.getenv('ACCESS_TOKEN'),
      'User-Agent': 'ZentralGut.ch',
      'Content-type': 'application/json'
  }
else:
    headers = {}

def getLanguage(textString):
    #file = 'File:ZHB_SoSa_Portrait_127_cropped.jpg'
    base_url = 'https://api.wikimedia.org/service/lw/inference/v1/models/langid:predict'
    data = {"text": ''.join(textString.splitlines())}
    response = requests.post(base_url, headers=headers, data=json.dumps(data))
    return (response.json())

if __name__ == '__main__':
    print(getLanguage("""tri de Gundoldingrn, Equis, & Reipublica camis XXXVI. Confulis digniffimi manes, pro
atria, pro Libertate, profoederatis amicis in prællo illo Sempaćemi generofe ac fponte i"""))