import requests
import json
import re
#https://commons.wikimedia.org/w/api.php?
##action=query&
##format=json&
##prop=&
##list=categorymembers&
##formatversion=2&
##cmtitle=Category%3APortr%C3%A4tgalerie%20der%20merkw%C3%BCrdigen%20Luzernerinnen%20und%20Luzerner%2FCropped%20Portraits
##cmlimit=500


def getCroppedPortraits():

    apiUrl = "https://commons.wikimedia.org/w/api.php"
    params = { "action" : "query", 
            "format" : "json", 
            "prop" : "list", 
            "list" : "categorymembers", 
            "formatversion" : "2", 
            "cmtitle" : "Category:Porträtgalerie der merkwürdigen Luzernerinnen und Luzerner/Cropped Portraits",
            "cmlimit" : "500"}

    response = requests.get(url=apiUrl, params=params)
    images = response.json()

    portraits = []
    regex = r"File:ZHB SoSa.*cropped.jpg"
    for image in images["query"]["categorymembers"]:
        matches = re.finditer(regex, image["title"], re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1): #To avoid having some other fance cropped images let's regex to the original cropped portraits
            portraits.append(match.group(0))
    
    return portraits

if __name__ == '__main__':
    print(getCroppedPortraits())